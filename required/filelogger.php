<?php

/**
 * File logger
 *
 * Log notices, warnings, errors or fatal errors into a log file.
 *
 * @author gehaxelt7
 * @author elena
 */
class FileLogger {
    /**
     * The file permissions.
     */
    protected const FILE_CHMOD = 756;

    public const NOTICE = [0, '[NOTICE]'];
    public const WARNING = [1, '[WARNING]'];
    public const ERROR = [2, '[ERROR]'];
    public const FATAL = [3, '[FATAL]'];

    /**
     * Holds the file handle.
     *
     * @var resource
     */
    protected $fileHandle = NULL;

    /**
     * The time format to show in the log.
     *
     * @var string
     */
    protected $timeFormat = 'd.m.Y - H:i:s';

    /**
     * The minimum message priority to log.
     *
     * @var int
     */
    protected $minMessagePriority = self::NOTICE[0];

    /**
     * Opens the file handle.
     *
     * @param string $logfile The path to the loggable file.
     * @param array $messageType Minimum message type.
     */
    public function __construct($logfile, $minMessageType) {
        $this->minMessagePriority = $minMessageType[0];
        if($this->fileHandle == NULL){
            $this->openLogFile($logfile);
        }
    }

    /**
     * Closes the file handle.
     */
    public function __destruct() {
        $this->closeLogFile();
    }

    /**
     * Logs the message into the log file.
     *
     * @param  string $message The log message.
     * @param  int$messageType Optional: urgency of the message.
     */
    public function log($message, $messageType = self::WARNING) {
        if($this->fileHandle == NULL){
            throw new Exception('Logfile is not opened.');
        }

        if(!is_string($message)){
            throw new Exception('$message is not a string');
        }

        if($messageType[0] >= $this->minMessagePriority){
            $type = $messageType[1];
            if($type != self::NOTICE[1] &&
               $type != self::WARNING[1] &&
               $type != self::ERROR[1] &&
               $type != self::FATAL[1]
            ){
                throw new Exception('Wrong $messagetype given.' . $type);
            }

            $this->writeToLogFile("[".$this->getTime()."]".$type." - ".$message);
        }
    }

    /**
     * Writes content to the log file.
     *
     * @param string $message
     */
    private function writeToLogFile($message) {
        flock($this->fileHandle, LOCK_EX);
        fwrite($this->fileHandle, $message.PHP_EOL);
        flock($this->fileHandle, LOCK_UN);
    }

    /**
     * Returns the current timestamp.
     *
     * @return string with the current date
     */
    private function getTime() {
        return date($this->timeFormat);
    }

    /**
     * Closes the current log file.
     */
    protected function closeLogFile() {
        if($this->fileHandle != NULL) {
            fclose($this->fileHandle);
            $this->fileHandle = NULL;
        }
    }

    /**
     * Opens a file handle.
     *
     * @param string $logFile Path to log file.
     */
    public function openLogFile($logFile) {
        $this->closeLogFile();

        if(!is_dir(dirname($logFile))){
            if(!mkdir(dirname($logFile), self::FILE_CHMOD, true)){
                throw new Exception('Could not find or create directory for log file.');
            }
        }

        if(!$this->fileHandle = fopen($logFile, 'a+')){
            throw new Exception('Could not open file handle.');
        }
    }

}