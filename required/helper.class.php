<?php
// Contains helper functions, that don't belong elsewhere
class Helper {
    private static $Instance;

    // Returns current instance
    public static function Get() {
        if(!isset(self::$Instance))
            self::$Instance = new Helper();

        return self::$Instance;
    }

    public $Database;
    public $Log;
    public $LoggedUser;

    public function Initialize() {
        $this->Database = new SafeMySQL();
        $this->Log = new FileLogger(
            MAIN_DIRECTORY . "/logs/log.txt",
            DEBUG_MODE ? FileLogger::NOTICE : FileLogger::ERROR
        );
    }

    // Returns true, if no error occurred
    // Return false, if an error occurred
    public function ValidatePostRequest(string $class_name) {
        $validator = new $class_name();

        $result = $validator->Validate();
        if(!$result[0]->Success)
            $_SESSION["error_message"] = $result[0]->ErrorMessage . ": " . $result[1]->ErrorMessage;

        return $result[0]->Success;
    }
}