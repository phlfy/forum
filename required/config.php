<?php
define("FORUM_NAME",     "TestForum");
define("DEBUG_MODE",     true);
define("MAIN_DIRECTORY", "C:/Program Files (x86)/EasyPHP-Devserver-17/eds-www");

// MySQL details
define("SQLHOST",  "127.0.0.1"); // MySQL host IP/FQDN
define("SQLUSER",  "root"); // MySQL login username
define("SQLPASS",  ""); // MySQL login password
define("SQLDB",    "forum"); // MySQL database to use
define("SQLPORT",  3306); // MySQL port to connect on
define("SQLSOCK",  "/var/run/mysqld/mysqld.sock");

// Add document root path to the include path.
// Because require_once doesn't work with relative paths,
// and autoloading is out-of-scope for this project.
set_include_path(get_include_path() . PATH_SEPARATOR . $_SERVER['DOCUMENT_ROOT']);

if(DEBUG_MODE) {
    ini_set('error_reporting', E_ALL);
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
}