<?php
require_once "validator_interface.class.php";

class RegisterValidator extends IValidatorInterface {
    // Format: Parameter => Value
    private $Parameters = [
        "email" => "",
        "nickname" => "",
        "password" => ""
    ];

    private function AreAllParametersValid() {
        foreach($this->Parameters as $key => $value) {
            if(!$this->IsParameterValid($key))
                return new ValidationResult(false, "Parameter `$key` does not conform to a value");

            $this->Parameters[$key] = $_POST[$key];
        }

        return new ValidationResult(true);
    }

    protected function IsValidEmail(string& $email) {
        if(strlen($email) < 8)
            return new ValidationResult(false, "Email too short");

        if(strlen($email) > 64)
            return new ValidationResult(false, "Email too long");

        if(!filter_var($email, FILTER_VALIDATE_EMAIL))
            return new ValidationResult(false, "Invalid email address");

        if(filter_var($email, FILTER_SANITIZE_EMAIL) != $email)
            return new ValidationResult(false, "Unsanitized email addresses are not accepted");

        return new ValidationResult(true);
    }

    protected function IsValidNickname(string $nickname) {
        if(strlen($nickname) < 4)
            return new ValidationResult(false, "Nickname too short");

        if(strlen($nickname) > 32)
            return new ValidationResult(false, "Nickname too long");

        // Allow only ASCII characters in the username
        if(!mb_check_encoding($nickname, "ASCII"))
            return new ValidationResult(false, "Nickname must consist only of ASCII characters");

        return new ValidationResult(true);
    }

    protected function IsValidPassword(string $password) {
        if(strlen($password) < 8)
            return new ValidationResult(false, "Password too short");

        if(strlen($password) > 64)
            return new ValidationResult(false, "Password too long");

        return new ValidationResult(true);
    }

    // Format: [Error message, error description]
    public function Validate() {
        $result = $this->AreAllParametersValid();
        if(!$result->Success)
            return [new ValidationResult(false, "Not all parameters are valid"), $result];

        $result = $this->IsValidEmail($this->Parameters["email"]);
        if(!$result->Success)
            return [new ValidationResult(false, "Email parameter is invalid"), $result];

        $result = $this->IsValidNickname($this->Parameters["nickname"]);
        if(!$result->Success)
            return [new ValidationResult(false, "Nickname parameter is invalid"), $result];

        $result = $this->IsValidPassword($this->Parameters["password"]);
        if(!$result->Success)
            return [new ValidationResult(false, "Password is invalid"), $result];

        // Success
        return [new ValidationResult(true), new ValidationResult(true)];
    }
}