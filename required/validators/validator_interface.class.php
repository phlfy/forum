<?php
class ValidationResult {
    public function __construct(bool $success, string $error_message = "") {
        $this->Success = $success;
        $this->ErrorMessage = $error_message;
    }

    public $Success;
    public $ErrorMessage;
}

// Interface used to validate POST'ed inputs
abstract class IValidatorInterface {
    abstract public function Validate();

    protected static function IsParameterValid(string $parameter) {
        return isset($_POST[$parameter]) && !empty($_POST[$parameter]);
    }
}