<?php
require_once "register_validator.class.php";

class LoginValidator extends RegisterValidator {
    // Format: Parameter => Value
    private $Parameters = [
        "email" => "",
        "password" => ""
    ];

    private function AreAllParametersValid() {
        foreach($this->Parameters as $key => $value) {
            if(!$this->IsParameterValid($key))
                return new ValidationResult(false, "Parameter \"$key\" does not conform to a value");

            $this->Parameters[$key] = $_POST[$key];
        }

        return new ValidationResult(true);
    }

    // Format: [Error message, error description]
    public function Validate() {
        $result = $this->AreAllParametersValid();
        if(!$result->Success)
            return [new ValidationResult(false, "Not all parameters are valid"), $result];

        $result = $this->IsValidEmail($this->Parameters["email"]);
        if(!$result->Success)
            return [new ValidationResult(false, "Email parameter is invalid"), $result];

        $result = $this->IsValidPassword($this->Parameters["password"]);
        if(!$result->Success)
            return [new ValidationResult(false, "Password is invalid"), $result];

        // Success
        return [new ValidationResult(true), new ValidationResult(true)];
    }
}