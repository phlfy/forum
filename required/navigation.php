<?php
require_once "views/render_interface.class.php";
require_once "views/index_view.class.php";
require_once "views/login_view.class.php";
require_once "views/register_view.class.php";
require_once "views/thread_view.class.php";
require_once "views/create_thread_view.class.php";

abstract class PageList {
    const Index = 0;
    const Login = 1;
    const Register = 2;
    const Thread = 3;
    const CreateThread = 4;
}

class Page {
    public function __construct(string $title, IRenderInterface $view, string $path) {
        $this->Title = $title;
        $this->View = $view;
        $this->Path = $path;
    }

    public $Title;
    public $View;
    public $Path;
}

// A neat way to use the Singleton pattern
class Navigation {
    private static $Instance;

    // Returns current instance
    public static function Get() {
        if(!isset(self::$Instance))
            self::$Instance = new Navigation();

        return self::$Instance;
    }

    // Defined in the following order:
    // Page name => [ Page title, Page template, Page path ]
    private $Pages = [];
    private $CurrentPageIndex = 0;

    // Populates the $Pages member with a list of available pages
    public function __construct() {
        $this->Pages = [
            PageList::Index => new Page("Index", new IndexView(), "/index.php"),
            PageList::Login => new Page("Login", new LoginView(), "/login.php"),
            PageList::Register => new Page("Register", new RegisterView(), "/register.php"),
            PageList::Thread => new Page("Thread", new ThreadView(), "/thread.php"),
            PageList::CreateThread => new Page("Create a thread", new CreateThreadView(), "/create_thread.php")
        ];
    }

    // Sets the current page index, so we can keep track on which page are we on
    public function SetCurrentPage(int $page_index) {
        $this->CurrentPageIndex = $page_index;
    }

    // Returns the page title of the given page
    public function GetPage(int $page_index) {
        return $this->Pages[$page_index];
    }

    // Returns the title of the current page
    public function GetCurrentPage() {
        return $this->GetPage($this->CurrentPageIndex);
    }
}