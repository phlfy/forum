<?php
// Ties other classes and functions used all over the site
require_once "config.php";

session_start(); // Start a session
ob_start(); // Start a buffer, mainly in case there is a MySQL error

require_once "safemysql.class.php";
require_once "helper.class.php";
require_once "filelogger.php";

Helper::Get()->Initialize();