<?php
require_once "render_interface.class.php";

class LoginView extends IRenderInterface {
    public function RenderContent() {
        require_once MAIN_DIRECTORY . "/static/login.html";
    }

    public function RenderHeader() {
        require_once MAIN_DIRECTORY . "/static/login_header.html";
    }

    public function RenderFooter() {
        require_once MAIN_DIRECTORY . "/static/login_footer.html";
    }
}