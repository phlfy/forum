<?php
// NOTE: Included files should be encoded in UTF-8 without BOM
abstract class IRenderInterface {
    abstract public function RenderContent();

    abstract public function RenderHeader();
    abstract public function RenderFooter();

    // This is the only function that should be directly used.
    public function Render() {
        $this->RenderHeader();
        $this->RenderContent();
        $this->RenderFooter();
    }
}