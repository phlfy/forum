<?php
require_once "render_interface.class.php";

class IndexView extends IRenderInterface {
    public function RenderContent() {
        require_once MAIN_DIRECTORY . "/static/index.html";
    }

    public function RenderHeader() {
        require_once MAIN_DIRECTORY . "/static/index_header.html";
    }

    public function RenderFooter() {
        require_once MAIN_DIRECTORY . "/static/index_footer.html";
    }
}