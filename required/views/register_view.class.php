<?php
require_once "render_interface.class.php";
require_once "login_view.class.php";

require_once "required/helper.class.php";
require_once "required/validators/register_validator.class.php";

class RegisterView extends LoginView {
    private function AddToDatabase() {
        // Hash password using BCRYPT algorithm
        $password = password_hash($_POST["password"], PASSWORD_BCRYPT);

        try {
            // Check if a user with the same email exists
            // NOTE: A "SELECT EXISTS(subquery)" query should be used, if available
            if(Helper::Get()->Database->getRow(
                "SELECT id FROM users WHERE email = ?s",
                $_POST["email"]
            )) {
                $_SESSION["error_message"] = "A user with this email address already exists.";
                return;
            }

            // Check if a user with the same nickname exists
            if(Helper::Get()->Database->getRow(
                "SELECT id FROM users WHERE nickname = ?s",
                $_POST["nickname"]
            )) {
                $_SESSION["error_message"] = "A user with this nickname already exists.";
                return;
            }

            // Try to add user into database. Shouldn't actually fail.
            if(!Helper::Get()->Database->query(
                "INSERT INTO users (email, nickname, password) VALUES (?s, ?s, ?s)",
                $_POST["email"], $_POST["nickname"], $password
            )) {
                $_SESSION["error_message"] = "Failed to add new user to database!";
                Helper::Get()->Log->log($_SESSION["error_message"], FileLogger::ERROR);
                return;
            }

            // Save these values when we redirect to the login page
            $_SESSION["email"] = $_POST["email"];
            $_SESSION["registration_message"] = "Your account has been successfully created. Please log in to continue.";

            header("Location: " . Navigation::Get()->GetPage(PageList::Login)->Path);
            die();
        }
        catch(Exception $e) {
            $_SESSION["error_message"] = $e->getMessage();
        }
    }

    public function RenderContent() {
        // Process parameters only on form submission
        if($_SERVER["REQUEST_METHOD"] == "POST") {
            // Make sure the request validates first, then process it
            if(Helper::Get()->ValidatePostRequest("RegisterValidator"))
                $this->AddToDatabase();
        }

        // Render content
        require_once MAIN_DIRECTORY . "/static/register.html";
    }
}