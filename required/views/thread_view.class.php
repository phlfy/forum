<?php
require_once "render_interface.class.php";

class ThreadView extends IRenderInterface {
    public function RenderContent() {
        require_once MAIN_DIRECTORY . "/static/thread.html";
    }

    public function RenderHeader() {
        require_once MAIN_DIRECTORY . "/static/thread_header.html";
    }

    public function RenderFooter() {
        require_once MAIN_DIRECTORY . "/static/thread_footer.html";
    }
}