<?php
require_once "thread_view.class.php";

class CreateThreadView extends ThreadView {
    public function RenderContent() {
        require_once MAIN_DIRECTORY . "/static/create_thread.html";
    }
}