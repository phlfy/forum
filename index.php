<?php
require_once "required/navigation.php";

Navigation::Get()->SetCurrentPage(PageList::Index);

// This script is intentionally loaded on every request
require_once "required/script_start.php";

// Render page
Navigation::Get()->GetCurrentPage()->View->Render();