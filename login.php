<?php
require_once "required/navigation.php";

Navigation::Get()->SetCurrentPage(PageList::Login);

// This script is intentionally loaded on every request
require_once "required/script_start.php";
require_once "required/validators/login_validator.class.php";
require_once "required/helper.class.php";

// This is very primitive. Sessions should be tracked via the database and heartbeats to ensure proper sign-on state.
function TryToAuthenticate() {
    // Acquire the nickname and then the password hash to verify with password_verify()
    $query = Helper::Get()->Database->getRow(
        "SELECT nickname, password FROM users WHERE email = ?s",
        $_POST["email"]
    );

    // Check if an entry with this email address in the database exists
    if(!$query) {
        $_SESSION["error_message"] = "A user with this email address doesn't exist";
        return;
    }

    // Check if the password hashes match
    if(!password_verify($_POST["password"], $query["password"])) {
        $_SESSION["error_message"] = "Incorrect password";
        return;
    }

    // Set appropriate credentials for this user
    $_SESSION["authenticated"] = true;
    $_SESSION["nickname"] = $query["nickname"];

    header("Location: " . Navigation::Get()->GetPage(PageList::Index)->Path);
    die();
}

// Process parameters only on form submission
if($_SERVER["REQUEST_METHOD"] == "POST") {
    // Make sure the request validates first, then process it
    if(Helper::Get()->ValidatePostRequest("LoginValidator"))
        TryToAuthenticate();
}

// Render page
Navigation::Get()->GetCurrentPage()->View->Render();